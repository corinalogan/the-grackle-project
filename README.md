# The Grackle Project

## How to increase safety when using the bownet trapping method
 - Bownet deployment [protocol](https://gitlab.com/corinalogan/the-grackle-project/blob/master/GrackleBownetTrapDeploymentProtocol.pdf)
 - Bownet training [checklist](https://gitlab.com/corinalogan/the-grackle-project/blob/master/Bownet_training_checklist.pdf)
 - Bownet training [tracking](https://gitlab.com/corinalogan/the-grackle-project/blob/master/BownetTrainingTracking.ods) data sheet
 - [Poster](https://gitlab.com/corinalogan/the-grackle-project/blob/master/2019-06-24_AOS_poster_bownet_modification.pdf) presented by Luisa Bergeron at the American Ornithological Society meeting in June 2019 in Alaska

**Citation:** Bergeron L & Logan CJ. 2019. Modifications to the bownet trapping method to increase safety for medium-sized, agile birds. American Ornithological Society Meeting, Alaska.

## Protocols
 - [Nest checks](https://gitlab.com/corinalogan/the-grackle-project/blob/master/protocolNestCheck.pdf) to collect indicators of nest building, egg laying behavior, nestling and fledgling presence, and nest failure
 - [Biometrics](https://gitlab.com/corinalogan/the-grackle-project/blob/master/protocolBiometrics.pdf)